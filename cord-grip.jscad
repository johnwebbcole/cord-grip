// title      : cordGrip
// author     : John Cole
// license    : ISC License
// file       : cordGrip.jscad

/* exported main, getParameterDefinitions */


function getParameterDefinitions() {
  return [
    { name: 'height', type: 'float', initial: 5.0, caption: 'height' },
    { name: 'thickness', type: 'float', initial: 2.1, caption: 'thickness' },
    { name: 'diameter1', type: 'float', initial: 4.25, caption: 'diameter 1' },
    { name: 'diameter2', type: 'float', initial: 3.25, caption: 'diameter 2' },
    { name: 'diameter3', type: 'float', initial: 0.0, caption: 'diameter 3' },
    { name: 'roundRadius', type: 'float', initial: 0.5, caption: 'round' },
    {
      name: 'resolution',
      type: 'choice',
      values: [0, 1, 2, 3, 4],
      captions: [
        'low (8,24)',
        'normal (12,32)',
        'high (24,64)',
        'very high (48,128)',
        'ultra high (96,256)',
      ],
      default: 0,
      initial: 0,
      caption: 'Resolution:',
    },
    { type: 'group', name: 'View' },
    {
      name: 'center',
      type: 'checkbox',
      checked: false,
    },
    { type: 'group', name: 'Cutaway' },
    {
      name: 'cutawayEnable',
      caption: 'Enable:',
      type: 'checkbox',
      checked: false,
    },
    {
      name: 'cutawayAxis',
      type: 'choice',
      values: ['x', 'y', 'z'],
      initial: 'y',
      caption: 'Axis:',
    },
  ];
}

function main(params) {
  var start = performance.now();
  var resolutions = [
    [6, 16],
    [8, 24],
    [12, 32],
    [24, 64],
    [48, 128],
  ];
  CSG.defaultResolution3D = resolutions[params.resolution][0];
  CSG.defaultResolution2D = resolutions[params.resolution][1];
  util.init(CSG, {
    debug: '*',
  });
  console.log('params', params);
  var thickness = util.nearest.over(params.thickness);
  console.log('thickeness', thickness);

  var Parts = {
    'tri-clip': () =>
      TriClip(
        thickness,
        params.diameter1,
        params.diameter2,
        params.diameter3,
        params.height
      ),
    'bi-clip': () =>
      BiClip(thickness, params.diameter1, params.diameter2, params.height),
  };

  var selectedParts = Parts[params.diameter3 > 0 ? 'tri-clip' : 'bi-clip']();
  // var selectedParts = Object.entries(parts)
  //   .filter(([key, value]) => {
  //     return params[key];
  //   })
  //   .reduce((parts, [key, value]) => {
  //     var part = value();
  //     if (Array.isArray(part)) parts = parts.concat(part);
  //     else parts.push(part);
  //     return parts;
  //   }, []);

  console.log('selectedParts', selectedParts);
  var parts = selectedParts.length ? selectedParts : [util.unitAxis(20)];

  if (params.center) parts = [union(parts).Center()];
  if (params.cutawayEnable)
    parts = [util.bisect(union(parts), params.cutawayAxis).parts.positive];

  console.log('timer', performance.now() - start);
  // console.log(
  //   'BOM\n',
  //   Object.entries(Hardware.BOM)
  //     .map(([key, value]) => `${key}: ${value}`)
  //     .join('\n')
  // );

  console.log('parts', ...parts);

  // return [...parts.map(p => p.combine())];
  var result = union(parts.map(p => p.parts.clamp)).fillet(params.roundRadius, 'z+').fillet(0.5, 'z-').subtract(parts.map(p => p.holes[0])).color('blue');
  console.log('result', result);
  return result;
  // return union([...parts.map(p => p.parts.clamp)])
}

function BiClip(thickness, diameter1 = 3.25, diameter2 = 3.25, height = 5, round = 1) {
  var center = util.unitCube(0.01, 0.01);
  var clamp = Clamp(diameter1, diameter1 + thickness, height, 90)
    .snap('clamp', center, 'y', 'outside-', -thickness / 4)
  var clamp2 = Clamp(diameter2, diameter2 + thickness, height, 90)
    .snap('clamp', center, 'y', 'outside-', -thickness / 4)
    .rotate(center, 'z', 180);

  return [clamp, clamp2];
}

// function Clamp(inner, outer, height, gapAngle) {
//   var clamp = Parts.Cylinder(outer, height).subtract(
//     Parts.Cylinder(inner, height)
//   );

//   var halfGapAngle = gapAngle / 2;
//   var a = wedge(clamp, halfGapAngle, -halfGapAngle);
//   return a.parts.body;
// }

function TriClip(
  thickness,
  diameter1 = 4.25,
  diameter2 = 3.25,
  diameter3 = 3.25,
  height = 5
) {
  var center = util.unitCube(0.01, 0.01);
  var clamp = Clamp(diameter1, diameter1 + thickness, height, 90)
    .snap('clamp', center, 'y', 'outside-')
    .rotate(center, 'z', 60);
  var clamp2 = Clamp(diameter2, diameter2 + thickness, height, 90)
    .snap('clamp', center, 'y', 'outside-')
    .rotate(center, 'z', -60);
  var clamp3 = Clamp(diameter3, diameter3 + thickness, height, 90)
    .snap('clamp', center, 'y', 'outside-')
    .rotate(center, 'z', 180);

  return [clamp, clamp2, clamp3]
}

function Clamp(inner, outer, height, gapAngle) {
  var g = Group()
  var clamp = Parts.Cylinder(outer, height); /* .subtract(
    Parts.Cylinder(inner, height)
  ); */
  var core = Parts.Cylinder(inner, height);

  var halfGapAngle = gapAngle / 2;
  var a = wedge(clamp, halfGapAngle, -halfGapAngle);
  g.add(a.parts.body, 'clamp');
  g.holes.push(core);
  // return a.parts.body;
  return g;
}

function wedge(object, start, end, axis) {
  var a = util.slice(object, start, axis);
  var b = util.slice(a.parts.positive, end, axis);
  return util.group({
    body: b.parts.positive.union(a.parts.negative).color('blue'),
    wedge: b.parts.negative.color('red'),
  });
}

// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// endinject
